import React, {useState, useEffect} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import s from './favorites.module.scss';
import {useDispatch} from "react-redux";
import {openModal} from "../../modalActions";

const FavoritesList = ({setFavoritesCount}) => {
  const [favoriteItems, setFavoriteItems] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavoriteItems(favorites);
  }, []);

  const removeFromFavorites = (sku) => {
    const newCart = favoriteItems.filter(product => product.sku !== sku);
    setFavoriteItems(newCart);
    setFavoritesCount(newCart.length)
    localStorage.setItem('favorites', JSON.stringify(newCart));
  }
  const openModalHandler = () => {
    dispatch(openModal());
  }

  return (
    <div className={s.wrap}>
      {favoriteItems.map((product) => (
        <ProductCard
          key={product.sku}
          openModalHandler={openModalHandler}
          product={product}
          removeFromFavorites={removeFromFavorites}/>
      ))}
    </div>
  );
};
export default FavoritesList;