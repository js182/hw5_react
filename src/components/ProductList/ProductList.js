import React from 'react';
import ProductCard from '../ProductCard/ProductCard';
import s from "./productList.module.scss";
import {useSelector, useDispatch} from "react-redux";
import {openModal} from "../../modalActions";

const ProductList = ({setCartCount, setFavoritesCount}) => {
  const data = useSelector((state) => state.products.products);
  const dispatch = useDispatch();

  const removeFromFavorites = (sku) => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    const newCart = favorites.filter(product => product.sku !== sku);
    setFavoritesCount(newCart.length);
    localStorage.setItem('favorites', JSON.stringify(newCart));
  }

  const openModalHandler = () => {
    dispatch(openModal());
  }

  return (
    <div className={s.wrap}>
      {data.map((p) => (
        <ProductCard
          setCartCount={setCartCount}
          key={p.sku}
          product={p}
          removeFromFavorites={removeFromFavorites}
          setFavoritesCount={setFavoritesCount}
          openModalHandler={openModalHandler}
        />
      ))}
    </div>
  );
};

export default ProductList;