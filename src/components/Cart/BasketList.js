import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {openModal} from '../../modalActions';
import {Formik, Field, Form, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import ProductCard from '../ProductCard/ProductCard';
import s from './cart.module.scss';

const BasketList = ({setCartCount}) => {
  const [cartItems, setCartItems] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cart);
  }, []);

  const handleRemoveFromCart = (skuToRemove) => {
    const newCart = cartItems.filter((product) => product.sku !== skuToRemove);
    setCartItems(newCart);
    setCartCount(newCart.length);
    localStorage.setItem('cart', JSON.stringify(newCart));
  };

  const openModalHandler = () => {
    dispatch(openModal());
  };
  const validationSchema = Yup.object({
    firstName: Yup.string().required('Please enter your first name'),
    lastName: Yup.string().required('Please enter your last name'),
    age: Yup.number().required('Please enter your age'),
    deliveryAddress: Yup.string().required('Please enter your delivery address'),
    phoneNumber: Yup.number().required('Please enter your phone number'),
  });
 const handleSubmit = (values, {resetForm}) => {
   console.log("Form submitted:", values);
   // Очистка формы после отправки
   resetForm();
   // Очистка корзины
   setCartItems([]);
   setCartCount(0);
   localStorage.removeItem("cart");
   // Вывод информации о приобретенных товарах и заполненных данных в консоль
   console.log("Purchased items:", cartItems);
   console.log("User info:", values);
 }
  return (
    <div className={s.wrap}>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          age: '',
          deliveryAddress: '',
          phoneNumber: ''
        }}
        validationSchema={validationSchema}
        onSubmit={(values, {resetForm}) => {
          console.log('Form submitted:', values);
          // Очистка формы после отправки
          resetForm();
          // Очистка корзины
          setCartItems([]);
          setCartCount(0);
          localStorage.removeItem('cart');

        }}
      >
        <Form className={s.form}>
          <div className={s.field}>
            <label htmlFor="firstName" className={s.label}>
              First Name:
            </label>
            <Field type="text" id="firstName" name="firstName" className={s.input} />
            <ErrorMessage name="firstName" component="div" className={s.error} />
          </div>
          <div className={s.field}>
            <label htmlFor="lastName" className={s.label}>
              Last Name:
            </label>
            <Field type="text" id="lastName" name="lastName" className={s.input} />
            <ErrorMessage name="lastName" component="div" className={s.error} />
          </div>
          <div className={s.field}>
            <label htmlFor="age" className={s.label}>
              Age:
            </label>
            <Field type="number" id="age" name="age" className={s.input} />
            <ErrorMessage name="age" component="div" className={s.error} />
          </div>
          <div className={s.field}>
            <label htmlFor="deliveryAddress" className={s.label}>
              Delivery Address:
            </label>
            <Field type="text" id="deliveryAddress" name="deliveryAddress" className={s.input} />
            <ErrorMessage name="deliveryAddress" component="div" className={s.error} />
          </div>
          <div className={s.field}>
            <label htmlFor="phoneNumber" className={s.label}>
              Phone Number:
            </label>
            <Field type="text" id="phoneNumber" name="phoneNumber" className={s.input} />
            <ErrorMessage name="phoneNumber" component="div" className={s.error} />
          </div>
          <button type="submit" className={s.button}>Checkout</button>
        </Form>
      </Formik>

      {cartItems.map((product) => (
        <ProductCard
          notAddCard
          key={product.sku}
          product={product}
          openModalHandler={openModalHandler}
          removeFromCart={handleRemoveFromCart}
        />
      ))}
    </div>
  );
};

export default BasketList;
