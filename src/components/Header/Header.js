import React, {useEffect, useState} from 'react';
import {ReactComponent as CartIcon} from '../../svg/basket.svg';
import {ReactComponent as StarIcon} from '../../svg/favorites.svg';
import {Link} from 'react-router-dom';
import s from "./header.module.scss";

const Header = () => {
  const [cartCount, setCartCount] = useState(0);
  const [favoritesCount, setFavoritesCount] = useState(0);

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartCount(cart.length);

    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavoritesCount(favorites.length);
  }, [localStorage.getItem('cart'), localStorage.getItem('favorites')]);

  const updateCartCount = () => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartCount(cart.length);
  };

  const updateFavoritesCount = () => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavoritesCount(favorites.length);
  };


  return (
    <header className={s.header}>
      <h1 className={s.prodact}>Product list</h1>
      <div className={s.iconsBlock}>
        <Link className={s.choice} to="/">
          <h2 className={s.page}>Product list</h2>
        </Link>
        <Link className={s.choice} to="/cart">
          <h2 className={s.page}>Корзина</h2>
        </Link>
        <Link className={s.choice} to="/favorites">
          <h2 className={s.page}>Избранное</h2>
        </Link>

        <div className={s.iconBlock}>
          <CartIcon className={s.icon}/>
          <span className={s.icon}>{cartCount}</span>
        </div>
        <div>
          <StarIcon className={s.icon}/>
          <span className={s.icon}>{favoritesCount}</span>
        </div>
      </div>

    </header>
  );
};

export default Header;